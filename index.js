(function () {
    var GiphyApp = angular.module("GiphyApp", []);

    //$http parameter is linked to "$http" from the injection line far below
    var GiphyCtrl = function ($http) {
        var giphyCtrl = this;
        giphyCtrl.subject = "";
        giphyCtrl.image = "https://www.jennybeaumont.com/wp-content/uploads/2015/03/placeholder.gif";
        giphyCtrl.search = function () {
            console.log("hello")
            if (!giphyCtrl.subject || (giphyCtrl.subject.trim().length <= 0))
                return;
            console.log("in search", giphyCtrl.subject);
            var promise = $http.get("https://api.giphy.com/v1/gifs/random", {
                params: {
                    api_key: "dc6zaTOxFJmzC",
                    tag: giphyCtrl.subject
                }
            });
            promise.then(function (result) {
                giphyCtrl.image = result.data.data.image_url;
            });
            promise.catch((status) => {
                // giphyCtrl.errorSTATUS = status;
                giphyCtrl.errorStatus = status.data.meta.status;
                giphyCtrl.errorMsg = status.data.meta.msg;
            });
        }
        // http://api.giphy.com/v1/gifs/search?q=funny+cat&api_key=dc6zaTOxFJmzC  
    };

    GiphyCtrl.$inject = ["$http"];

    GiphyApp.controller("GiphyCtrl", GiphyCtrl);
})();